$(document).ready(function(){
    var table =  $('#table').DataTable({
        "language": {
            "lengthMenu": "Prikaži _MENU_  po stranici",
            "zeroRecords": "Nema rezultata",
            "info": "Stranica _PAGE_ od _PAGES_",
            "infoEmpty": "Nema podataka",
            "infoFiltered": "(pretraženo od _MAX_ ukupnih zapisa)",
            "paginate": {
                "previous": "Predhodna",
                "next": "Sljedeća",
                "first": "Početna",
                "last": "Posljednja"
            },
            "search": "Pretraživanje"
        }
    });
	
	
})