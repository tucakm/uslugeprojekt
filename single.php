<?php
$active="single";
$title="singlepage";
include("core/header.php");

if(isset($_GET["id"])) {
    $sql = "SELECT usluge.*,korisnik.*,kategorije.* FROM usluge
JOIN korisnik ON korisnik.id_korisnika=usluge.id_korisnika
JOIN kategorije ON kategorije.id_kategorije=usluge.id_kategorije
WHERE aktivnost_usluge=1 AND id_usluge='" . $_GET["id"] . "'";

    if (!$single = mysqli_query($conn, $sql)) {
        echo "SQL nije prošao, error:" . mysqli_error($conn);
    }

    $rezz = mysqli_fetch_assoc($single);
    $name = "";
    if (empty($rezz["ime_korisnika"]) && empty($rezz["prezime_korisnika"])) {
        $name = $rezz["username_korisnika"];

    }
    else {

        $name = $rezz["ime_korisnika"] . " " . $rezz["prezime_korisnika"];
    }
}

?>
<!--===================================
=            Store Section            =
====================================-->
<section class="section bg-gray">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<!-- Left sidebar -->
			<div class="col-md-8">
				<div class="product-details">
					<h1 class="product-title"><?=$rezz["naziv_usluge"]?></h1>
					<div class="product-meta">
						<ul class="list-inline">
							<li class="list-inline-item"><i class="fa fa-user-o"></i> By <a href=uslugesearch.php?search=korisnik&id=<?=$rezz["id_korisnika"]?>><?=$name?></a></li>
							<li class="list-inline-item"><i class="fa fa-folder-open-o"></i>Kategorija <a href="uslugesearch.php?search=kategorije&naziv=<?=$rezz["naziv_kategorije"]?>"><?=$rezz["naziv_kategorije"]?></a></li>
						</ul>
					</div>
					<div id="carouselExampleIndicators" class="product-slider carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						</ol>
						<div class="carousel-inner">
							<div class="carousel-item active">
								<img class="d-block w-100" src="<?=$rezz["urlslike_usluge"]?>" alt="Slika">
							</div>

						</div>

					</div>
					<div class="content">
						<ul class="nav nav-pills  justify-content-center" id="pills-tab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="false">Opis usluge</a>
							</li>
						</ul>
						<div class="tab-content" >
							<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
								<h3 class="tab-title">Opis</h3>
								<?=$rezz["tekst_usluge"]?>

							</div>

							<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
								<h3 class="tab-title">Product Review</h3>
								<div class="product-review">
							  		<div class="media">
							  			<img src="images/user/user-thumb.jpg" alt="avater">
							  			<div class="media-body">
                                            <div class="ratings">
							  					<ul class="list-inline">
							  						<li class="list-inline-item">
							  							<i class="fa fa-star"></i>
							  						</li>
							  						<li class="list-inline-item">
							  							<i class="fa fa-star"></i>
							  						</li>
							  						<li class="list-inline-item">
							  							<i class="fa fa-star"></i>
							  						</li>
							  						<li class="list-inline-item">
							  							<i class="fa fa-star"></i>
							  						</li>
							  						<li class="list-inline-item">
							  							<i class="fa fa-star"></i>
							  						</li>
							  					</ul>
							  				</div>
							  				<div class="name">
							  					<h5>Jessica Brown</h5>
							  				</div>
							  				<div class="date">
							  					<p>Mar 20, 2018</p>
							  				</div>
							  				<div class="review-comment">
							  					<p>
							  						Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremqe laudant tota rem ape riamipsa eaque.
							  					</p>
							  				</div>
							  			</div>
							  		</div>
							  		<div class="review-submission">
							  			<h3 class="tab-title">Submit your review</h3>
						  				<!-- Rate -->
						  				<div class="rate">
						  					<div class="starrr"></div>
						  				</div>
						  				<div class="review-submit">
						  					<form action="#" class="row">
						  						<div class="col-lg-6">
						  							<input type="text" name="name" id="name" class="form-control" placeholder="Name">
						  						</div>
						  						<div class="col-lg-6">
						  							<input type="email" name="email" id="email" class="form-control" placeholder="Email">
						  						</div>
						  						<div class="col-12">
						  							<textarea name="review" id="review" rows="10" class="form-control" placeholder="Message"></textarea>
						  						</div>
						  						<div class="col-12">
						  							<button type="submit" class="btn btn-main">Sumbit</button>
						  						</div>
						  					</form>
						  				</div>
							  		</div>
							  	</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="sidebar">
					<div class="widget price text-center">
						<h4>Cijena</h4>
						<p><?=$rezz["cijena_usluge"]?> KN</p>
					</div>
					<!-- User Profile widget -->
					<div class="widget user">
						<h3><a href=""><?=$name?></a></h3>
						<p class="member-time"></p>
						<a href=uslugesearch.php?search=korisnik&id=<?=$rezz["id_korisnika"]?>>Vidi sve oglase</a>
						<ul class="list-inline mt-20">
                            <?php

                            $mailto="mailto:".$rezz["email_korisnika"];

                             echo'<li class="list-inline-item"><a href="'.$mailto.'" class="btn btn-contact">Kontaktiraj</a></li>'
                            ?>

						</ul>
					</div>


					<!-- Coupon Widget -->
					<div class="widget coupon text-center">
						<!-- Coupon description -->
						<p>Želite ponuditi uslugu?
						</p>
						<!-- Submii button -->
						<a href="login/index.php" class="btn btn-transparent-white">Ponudite uslugu</a>
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
	<!-- Container End -->
</section>
<!--============================
=            Footer            =
=============================-->

  <!-- JAVASCRIPTS -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="plugins/tether/js/tether.min.js"></script>
  <script src="plugins/raty/jquery.raty-fa.js"></script>
  <script src="plugins/bootstrap/dist/js/popper.min.js"></script>
  <script src="plugins/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="plugins/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
  <script src="plugins/slick-carousel/slick/slick.min.js"></script>
  <script src="plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
  <script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
  <script src="plugins/smoothscroll/SmoothScroll.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>
  <script src="js/scripts.js"></script>

</body>

</html>