<?php
$active="uslugesearch";
$title="usluge";
//include("core/header.php");
@session_start();
include("db.php");
$sql="";
if(isset($_GET["search"]) ){
  $sear='%'.$_GET["search"].'%';

    $sql="SELECT usluge.*,kategorije.* FROM usluge 
          JOIN kategorije ON kategorije.id_kategorije=usluge.id_kategorije
          WHERE kategorije.naziv_kategorije LIKE '$sear' OR usluge.naziv_usluge LIKE '$sear'";
    if($_GET["search"]=="kategorije"){
        $sear='%'.$_GET["naziv"].'%';
        $sql="SELECT usluge.*,kategorije.* FROM usluge 
          JOIN kategorije ON kategorije.id_kategorije=usluge.id_kategorije
          WHERE kategorije.naziv_kategorije LIKE '$sear' ";

    }
    if($_GET["search"]=="korisnik"){
        $sql="SELECT usluge.*,kategorije.* FROM usluge 
          JOIN kategorije ON kategorije.id_kategorije=usluge.id_kategorije          
          WHERE usluge.id_korisnika='". $_GET["id"] . "'";
    }
}
else{
    $sql="SELECT * FROM usluge 
          JOIN kategorije ON kategorije.id_kategorije=usluge.id_kategorije WHERE aktivnost_usluge=1 ";

    if (!$rezultat=mysqli_query($conn,$sql)) {
        echo "Select članka nije prošao , error: " . mysqli_error($conn);
    }
}

?>
<html lang="en">
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$title?></title>

    <!-- PLUGINS CSS STYLE -->
    <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="plugins/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
    <!-- Fancy Box -->
    <link href="plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
    <link href="plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
    <link href="plugins/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../DataTables/css/dataTables.bootstrap.min.css" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <!-- FAVICON -->
    <link href="img/favicon.png" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

</head>

<body class="body-wrapper">


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg  navigation">

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto main-nav ">
                            <li class="nav-item <?=$active=='home' ? 'active' : ''?>">
                                <a class="nav-link" href="index.php">Početna</a>
                            </li>
                            <li class="nav-item <?=$active=='uslugesearch' ? 'active' : ''?>">
                                <a class="nav-link" href="uslugesearch.php">Usluge</a>
                            </li>

                        </ul>
                        <ul class="navbar-nav ml-auto mt-10">
                            <?php
                            if(empty($_SESSION["id_korisnika"]))
                            {

                                echo '<li class="nav-item">';
                                echo '<a class="nav-link login-button" href="login/index.php">Login</a>';
                                echo '</li>';
                                echo '<li class="nav-item">';
                                echo '<a class="nav-link add-button" href="reg/index.php"><i class="fa fa-plus-circle"></i> Registriraj se</a>';
                                echo '</li>';
                            }
                            else{
                                echo '<li class="nav-item">';
                                echo '<a class="nav-link login-button" href="user-profile.php">Moj profil</a>';
                                echo '</li>';
                                echo '<li class="nav-item">';
                                echo '<a class="nav-link add-button" href="reg/logout.php"><i class="fa fa-plus-circle"></i> Odjavi se</a>';
                                echo '</li>';
                            }
                            ?>

                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>
<section class="page-search">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Advance Search -->

			</div>
		</div>
	</div>
</section>
<section class="section-sm">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

            </div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="category-sidebar">
					<div class="widget category-list">
	                        <h4 class="widget-header">Kategorije</h4>
	                                <ul class="category-list">
                                        <?php
                                        $SQL="SELECT * FROM kategorije";
                                        if (!$rezultat=mysqli_query($conn,$SQL)) {
                                                echo "Select članka nije prošao , error: " . mysqli_error($conn);
                                        }

                                        while($redci=mysqli_fetch_assoc($rezultat)){
                                            echo '<li><a href="uslugesearch.php?search=kategorije&naziv='.$redci["naziv_kategorije"].'">'.$redci["naziv_kategorije"].'</a></li>';

                                        }

                                        ?>
	                                </ul>
                    </div>


            </div>

          </div>
			<div class="col-md-9">
				<div class="category-search-filter">
					<div class="" >
                        <table class="table table-hover table-striped" id="table">

                            <thead>
                            <tr>
                                <th>Slika</th>
                                <th>Naziv usluge</th>
                                <th class="text-center">Kategorija</th>
                                <th class="text-center">Pregled</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sql1=$sql;

                            if (!$rezultat=mysqli_query($conn,$sql1)) {
                                echo "Select članka nije prošao , error: " . mysqli_error($conn);
                            }

                            while($thisredci=mysqli_fetch_assoc($rezultat)){
                                echo '<tr>';
                                echo '<td class="product-thumb">';
                                echo '<img width="80px" height="auto" src="'.$thisredci["urlslike_usluge"].'" alt="image description"></td>';
                                echo '<td class="product-details">';
                                echo '<h3 class="title">'.$thisredci["naziv_usluge"].'</h3>';
                                echo '<span class="add-id"><strong> ID:</strong>'.$thisredci["id_usluge"].'</span> <br>';

                                echo '<span class="location"><strong>Cijena</strong>'.$thisredci["cijena_usluge"].'</span><br>';
                                echo '</td>';
                                echo '<td class="product-category"><span class="categories">'.$thisredci["naziv_kategorije"].'</span></td>';
                                echo '<td class="action" data-title="Action">';
                                echo '<ul class="list-inline justify-content-center">';
                                echo '<li class="list-inline-item">';
                                echo ' <a data-toggle="tooltip" data-placement="top" title="Vidi oglas" class="view" href="single.php?id='.$thisredci["id_usluge"].'">';
                                echo '<i class="fa fa-eye"></i>';
                                echo '</a>';
                                echo '</li>';
                                echo '</ul>';

                                echo '</td>';
                                echo '</tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

			</div>
		</div>
	</div>
</section>
<!--============================
=            Footer            =
=============================-->

<script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var table =  $('#table').DataTable( {
            "language": {
                "lengthMenu": "Prikaži _MENU_  po stranici",
                "zeroRecords": "Nema rezultata",
                "info": "Stranica _PAGE_ od _PAGES_",
                "infoEmpty": "Nema podataka",
                "infoFiltered": "(pretraženo od _MAX_ ukupnih zapisa)",
                "paginate": {
                    "previous": "Predhodna",
                    "next": "Sljedeća",
                    "first": "Početna",
                    "last": "Posljednja"
                },
                "search": "Pretraživanje"
            }
        });

        $( table.table().container() ).removeClass( 'form-inline' );



    } );
</script>
<!--Data table-->
<script type="text/javascript"  src="DataTables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="DataTables/js/dataTables.bootstrap.min.js"></script>

<script src="assets/ckeditor/ckeditor.js" type="text/javascript"></script>



  <!-- JAVASCRIPTS -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="plugins/tether/js/tether.min.js"></script>
  <script src="plugins/raty/jquery.raty-fa.js"></script>
  <script src="plugins/bootstrap/dist/js/popper.min.js"></script>
  <script src="plugins/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="plugins/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
  <script src="plugins/slick-carousel/slick/slick.min.js"></script>
  <script src="plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
  <script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
  <script src="plugins/smoothscroll/SmoothScroll.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>
  <script src="js/scripts.js"></script>

</body>

</html>