<?php

$active="mojeusluge";
$title="MojeUsluge";
$target_dir="uploads/";



include("core/header.php");


if (!isset($_SESSION['id_korisnika'])) {
    header("Location: index.php");
    exit();
}

$sql="SELECT * FROM korisnik
WHERE aktivnost_korisnika=1 AND id_korisnika='". $_SESSION["id_korisnika"] . "'";


if (!$login=mysqli_query($conn, $sql)) {
    echo"SQL nije prošao, error:".mysqli_error($conn);
}
$redak = mysqli_fetch_assoc($login);
$edit=0;
if (isset($_GET["opcija"])) {

    if ($_GET["opcija"] == "brisanje") {
        $sql5 = "DELETE FROM usluge WHERE id_usluge='" . $_GET["id"] . "'";

        if (!$rezultat = mysqli_query($conn, $sql5)) {
            echo "Select članka nije prošao , error: " . mysqli_error($conn);
        }
       header("Location: myservices.php");
    }
    if ($_GET["opcija"] == "mijenjanje") {
        $sql6 = "SELECT * FROM usluge 
                WHERE id_usluge='" . $_GET["id"] . "'";

        if (!$rezultat = mysqli_query($conn, $sql6)) {
            echo "Select članka nije prošao , error: " . mysqli_error($conn);
        }
        $redak_clanak = mysqli_fetch_assoc($rezultat);


        $edit = 1;

    }
    if ($_GET["opcija"] == "objavi") {

        if (!empty($_FILES["file"]["type"])) {
            $fileName = time() . '_' . $_FILES['file']['name'];
            $valid_extensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $_FILES["file"]["name"]);
            $file_extension = end($temporary);
            if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)) {
                $sourcePath = $_FILES['file']['tmp_name'];
                $targetPath = $target_dir;
                if (move_uploaded_file($sourcePath, $targetPath . $fileName)) {
                    $uploadedFile = $fileName;
                }
            }
            $slika['url'] = $targetPath . $fileName;
            $_POST['slikaTeksta'] = $slika['url'];

        }

        $sql7 = "UPDATE usluge SET
            naziv_usluge='" . $_POST["naziv_usluge"] . "',
            cijena_usluge='" . $_POST["cijena_usluge"] . "'
            " . (isset($_POST['slikaTeksta']) ? ",
            urlslike_usluge='" . $_POST["slikaTeksta"] . "'" : '') . ",
            id_kategorije='" . $_POST["odabirKategorije"] . "'      
            WHERE id_usluge='" . $_GET["id_usluge"] . "'";

        if (!$rezultat = mysqli_query($conn, $sql7)) {
            echo "Select članka nije prošao , error: " . mysqli_error($conn);
        }
        /*if(!($newstmt = $conn->prepare("UPDATE usluge SET naziv_usluge=?,"))){
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }*/

        header("Location: myservices.php");

    }
    if ($_GET["opcija"] == "dodaj") {
        if(!empty($_FILES["file"]["type"])){
            $fileName = time().'_'.$_FILES['file']['name'];
            $valid_extensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $_FILES["file"]["name"]);
            $file_extension = end($temporary);
            if((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
                $sourcePath = $_FILES['file']['tmp_name'];
                $targetPath = $target_dir;
                if(move_uploaded_file($sourcePath,$targetPath.$fileName)){
                    $uploadedFile = $fileName;
                }
            }


            $slika['url'] = $targetPath.$fileName;
            $_POST['slikaTeksta']= $slika['url'];
        }

        if(!($newstmt = $conn->prepare("INSERT INTO usluge (naziv_usluge,cijena_usluge,tekst_usluge,id_kategorije,urlslike_usluge,id_korisnika) VALUES(?,?,?,?,?,?)"))){
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        $newstmt->bind_param ("sisisi",$_POST["naziv_usluge"],$_POST["cijena_usluge"],$_POST["tekst_usluge"],$_POST["odabirKategorije"],$_POST['slikaTeksta'],$_SESSION["id_korisnika"]);

        if (!$newstmt->execute()) {
            echo "Select članka nije prošao , error: " . mysqli_error($conn);
        }
       header("Location: myservices.php");
    }
}

?>

<section class="dashboard section">
	<!-- Container Start -->
	<div class="container">
		<!-- Row Start -->
		<div class="row">
			<div class="col-md-10 offset-md-1 col-lg-4 offset-lg-0">
				<div class="sidebar">
					<!-- User Widget -->
					<div class="widget user-dashboard-profile">
						<!-- User Image -->
						<!--<div class="profile-thumb">
							<img src="images/user/user-thumb.jpg" alt="" class="rounded-circle">
						</div>-->
						<!-- User Name -->
						<h5 class="text-center"><?=$redak["ime_korisnika"]." ".$redak["prezime_korisnika"]?></h5>
						<a href="user-profile.php" class="btn btn-main-sm">Uredi profil</a>
					</div>
					<!-- Dashboard Links -->
					<div class="widget user-dashboard-menu">
						<ul>
							<li class="active">
								<a href="myservices.php"><i class="fa fa-user"></i>Moje usluge</a>
                            </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-8  ">
				<!-- Recently Favorited -->
				<div class="widget dashboard-container my-adslist  ">
					<h3 class="widget-header">Moje Usluge</h3>
					<table class="table table-hover table-striped " id="table">
						<thead>
							<tr>
								<th>Slika</th>
								<th>Naziv usluge</th>
								<th class="text-center">Kategorija</th>
								<th class="text-center">Akcija</th>
							</tr>
						</thead>
						<tbody>
                        <?php
                        $sql="SELECT * FROM usluge 
                         JOIN kategorije ON kategorije.id_kategorije=usluge.id_kategorije WHERE aktivnost_usluge=1 AND
                         id_korisnika='" . $redak["id_korisnika"] . "'";

                        if (!$rezultat=mysqli_query($conn,$sql)) {
                            echo "Select članka nije prošao , error: " . mysqli_error($conn);
                        }

                        while($redci=mysqli_fetch_assoc($rezultat)){
                            echo '<tr>';
                            echo '<td class="product-thumb">';
                            echo '<img width="80px" height="auto" src="'.$redci["urlslike_usluge"].'" alt="image description"></td>';
                            echo '<td class="product-details">';
                            echo '<h3 class="title">'.$redci["naziv_usluge"].'</h3>';
                            echo '<span class="add-id"><strong> ID:</strong>'.$redci["id_usluge"].'</span> <br>';
                            echo '<span class="status active"><strong>Status</strong>Aktivna</span><br>';
                            echo '<span class="location"><strong>Cijena</strong>'.$redci["cijena_usluge"].'</span><br>';
                            echo '</td>';
                            echo '<td class="product-category"><span class="categories">'.$redci["naziv_kategorije"].'</span></td>';
                            echo '<td class="action " data-title="Action">';

                            echo '<ul class="list-inline justify-content-center">';
                            echo '<li class="list-inline-item">';
                            echo ' <a data-toggle="tooltip" data-placement="top" title="Vidi oglas" class="view" href="single.php?id='.$redci["id_usluge"].'">';
                            echo '<i class="fa fa-eye"></i>';
                            echo '</a>';
                            echo '</li>';
                            echo '';
                            echo '<li class="list-inline-item">';
                            echo '<a class="edit" title="Uredi oglas" href="myservices.php?opcija=mijenjanje&id='.$redci["id_usluge"].'"">';
                            echo '<i class="fa fa-pencil"></i>';
                            echo '</a>';
                            echo '</li>';
                            echo '<li class="list-inline-item">';
                            echo '<a class="delete" title="Izbriši oglas" href="myservices.php?opcija=brisanje&id='.$redci["id_usluge"].'"">';
                            echo '<i class="fa fa-trash"></i>';
                            echo '</a>';
                            echo '</li>';
                            echo '</ul>';

                            echo '</td>';
                            echo '</tr>';
                        }
                        ?>
						</tbody>
					</table>

				</div>
			</div>
            <div class="col-md-12">
                <div class="widget dashboard-container my-adslist">
                    <div >
                        <div class="header">
                            <h4 class="title"><?php
                                if($edit==1){
                                    echo 'Ažuriraj oglas';
                                }
                                else
                                    echo 'Dodaj novi oglas';


                                ?>
                            </h4>
                        </div>
                        <div class="content">
                            <form action="<?php
                            if ($edit==1){
                                echo "myservices.php?opcija=objavi&id_usluge=".$redak_clanak["id_usluge"];

                            }
                            else {
                                echo "myservices.php?opcija=dodaj";
                            }
                            ?>" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Naslov</label>
                                            <input type="text" class="form-control"  value="<?php
                                            if($edit==1){
                                                echo $redak_clanak["naziv_usluge"];
                                            }
                                            ?>" name="naziv_usluge" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Cijena usluge</label>
                                            <input type="number" class="form-control"  value="<?php
                                            if($edit==1){
                                                echo $redak_clanak["cijena_usluge"];
                                            }
                                            ?>" name="cijena_usluge" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                            <label>Kategorije<span>(Obavezno)</span></label>
                                            <select name="odabirKategorije" required >
                                                <?php
                                                $sql4="SELECT *FROM kategorije";
                                                if (!$rezultat=mysqli_query($conn,$sql4)) {
                                                    echo "Select članka nije prošao , error: " . mysqli_error($conn);
                                                }
                                                while($redaka=mysqli_fetch_assoc($rezultat)){
                                                    echo'<option value="'.$redaka["id_kategorije"].'">'.$redaka["naziv_kategorije"].'</option>';
                                                }
                                                ?>
                                            </select>
                                    </div>
                                </div>
                               <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Tekst</label>
                                            <textarea class="ckeditor" cols="80" id="editor1" name="tekst_usluge" rows="10" ><?php if($edit==1){echo $redak_clanak["tekst_usluge"];}?></textarea>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php
                                            if($edit==1){
                                                $path=$redak_clanak["urlslike_usluge"];
                                                echo "<img src='".$path."' width='200px'/>";
                                            }
                                            ?>
                                            <label>Slika</label>
                                            <input type="file" name="file" value="">
                                        </div>
                                    </div>
                                </div class="btn btn-info btn-fill pull-right" >

                                <button type="submit" class="btn btn-info btn-fill pull-right">Objavi</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
		</div>
		<!-- Row End -->
	</div>
	<!-- Container End -->
</section>

<script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var table =  $('#table').DataTable( {
            "language": {
                "lengthMenu": "Prikaži _MENU_  po stranici",
                "zeroRecords": "Nema rezultata",
                "info": "Stranica _PAGE_ od _PAGES_",
                "infoEmpty": "Nema podataka",
                "infoFiltered": "(pretraženo od _MAX_ ukupnih zapisa)",
                "paginate": {
                    "previous": "Predhodna",
                    "next": "Sljedeća",
                    "first": "Početna",
                    "last": "Posljednja"
                },
                "search": "Pretraživanje"
            }
        });

        $( table.table().container() ).removeClass( 'form-inline' );

    } );
</script>
    <!-- id tablice treba biti table "-->
<!-- DataTable -->
  <script type="text/javascript"  src="DataTables/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="DataTables/js/dataTables.bootstrap.min.js"></script>
  <script src="assets/ckeditor/ckeditor.js" type="text/javascript"></script>


</script>
  <script src="plugins/jquery/jquery.min.js"></script>
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="plugins/tether/js/tether.min.js"></script>
  <script src="plugins/raty/jquery.raty-fa.js"></script>
  <script src="plugins/bootstrap/dist/js/popper.min.js"></script>
  <script src="plugins/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="plugins/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
  <script src="plugins/slick-carousel/slick/slick.min.js"></script>
  <script src="plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
  <script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
  <script src="plugins/smoothscroll/SmoothScroll.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>
  <script src="js/scripts.js"></script>

</body>

</html>