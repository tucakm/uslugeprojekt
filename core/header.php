<?php
include("db.php");
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$title?></title>

    <!-- PLUGINS CSS STYLE -->
    <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="plugins/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
    <!-- Fancy Box -->
    <link href="plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
    <link href="plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
    <link href="plugins/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../DataTables/css/dataTables.bootstrap.min.css" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <!-- FAVICON -->
    <link href="img/favicon.png" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

</head>

<body class="body-wrapper">


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg  navigation">

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto main-nav ">
                            <li class="nav-item <?=$active=='home' ? 'active' : ''?>">
                                <a class="nav-link" href="index.php">Početna</a>
                            </li>
                            <li class="nav-item <?=$active=='uslugesearch' ? 'active' : ''?>">
                                <a class="nav-link" href="uslugesearch.php">Usluge</a>
                            </li>

                        </ul>
                        <ul class="navbar-nav ml-auto mt-10">
                            <?php
                            if(empty($_SESSION["id_korisnika"]))
                            {

                                echo '<li class="nav-item">';
                                echo '<a class="nav-link login-button" href="login/index.php">Login</a>';
                                echo '</li>';
                                echo '<li class="nav-item">';
                                echo '<a class="nav-link add-button" href="reg/index.php"><i class="fa fa-plus-circle"></i> Registriraj se</a>';
                                echo '</li>';
                            }
                            else{
                                echo '<li class="nav-item">';
                                echo '<a class="nav-link login-button" href="user-profile.php">Moj profil</a>';
                                echo '</li>';
                                echo '<li class="nav-item">';
                                echo '<a class="nav-link add-button" href="reg/logout.php"><i class="fa fa-plus-circle"></i> Odjavi se</a>';
                                echo '</li>';
                            }
                            ?>

                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>

<!--=======================