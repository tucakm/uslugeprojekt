$('document').ready(function()
{
    /* validation */
    $("#register-form").validate({
        rules:
        {
            user_name: {
                required: true,
                minlength: 3
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 15
            },
            cpassword: {
                required: true,
                equalTo: '#password'
            },
            user_email: {
                required: true,
                email: true
            },
        },
        messages:
        {
            user_name: "Unesit ispravno korisničko ime",
            password:{
                required: "Morate unijeti zaporku",
                minlength: "Zaporka treba biti minimalno 8 znakova"
            },
            user_email: "Unesite ispravan email",
            cpassword:{
                required: "Unesite ponovno zaporku",
                equalTo: "Zaporke nisu iste"
            }
        },
        submitHandler: submitForm
    });
  
    function submitForm()
    {
        var data = $("#register-form").serialize();

        $.ajax({

            type : 'POST',
            url  : 'register.php',
            data : data,
            beforeSend: function()
            {
                $("#error").fadeOut();
                $("#btn-submit").fadeOut();


            },
            success :  function(data)
            {
                if(data==1){

                    $("#error").fadeIn(1000, function(){
                        $("#btn-submit").fadeIn();

                        $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Korisnik s tim emailom već postoji!</div>');



                    });
                    $("#btn-submit").fadeIn(1000, function(){
                        $("#btn-submit").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Registrirajte se');
                    });

                }
                else if(data=="registered")
                {

                    $("#btn-submit").html('Signing Up');
                    setTimeout('$(".form-signin").fadeOut(500, function(){ $(".signin-form").load("successreg.php"); }); ',5000);

                }
                else{

                    $("#error").fadeIn(1000, function(){
                        $("#btn-submit").fadeIn();

                        $("#error").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+data+' !</div>');

                    });
                    $("#btn-submit").fadeIn(1000, function(){
                        $("#btn-submit").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Registrirajte se');
                    });

                }
            }
        });
        return false;
    }
    /* form submit */

});