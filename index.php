<?php
$title="Usluge" ;
$active = 'home';
include("core/header.php");


$sql="SELECT usluge.*, kategorije.* FROM usluge 
JOIN kategorije ON kategorije.id_kategorije=usluge.id_kategorije
ORDER BY id_usluge DESC
LIMIT 3 ";

if (!$login=mysqli_query($conn, $sql)) {
    echo"SQL nije prošao, error:".mysqli_error($conn);
}

?>

<section class="page-search">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Advance Search -->
                <div class="advance-search">
                    <form method="get" action="uslugesearch.php">
                        <div class="form-row">
                            <div class="form-group col-md-2 ">
                                <!--<input type="text" class="form-control" id="inputCategory4" placeholder="Category">-->
                            </div>
                            <div class="form-group col-md-7 ">
                                <input type="text" class="form-control" id="inputtext4" name="search" placeholder="Koju uslugu tražiš?">
                            </div>
                            <div class="form-group col-md-2">

                                <button type="submit" name="submit" class="btn btn-primary">Pretraži</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!--===================================
=            Client Slider            =
====================================-->


<!--===========================================
=            Popular deals section            =
============================================-->

<section class="popular-deals section bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<h2>Najnoviji oglasi</h2>
					<p></p>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- offer 01 -->

				<!-- product card -->
                <?php
                while($redak = mysqli_fetch_assoc($login)){
                    echo '<div class="col-sm-12 col-lg-4">';
                    echo '<div class="product-item bg-light">';
                    echo '<div class="card">';
                    echo '<div class="thumb-content">';
                    echo '<a href="single.php?id='.$redak["id_usluge"].'">';
                    echo '<img class="card-img-top img-fluid" src="'.$redak["urlslike_usluge"].'" alt="Card image cap">';
                    echo '</a>';
                    echo '</div>';
                    echo '<div class="card-body">';
                    echo '<h4 class="card-title"><a href="single.php?id='.$redak["id_usluge"].'">'.$redak["naziv_usluge"].'</a></h4>';
                    echo '<ul class="list-inline product-meta">';
                    echo '<li class="list-inline-item">';
                    echo '<a href=""><i class="fa fa-folder-open-o"></i>'.$redak["naziv_kategorije"].'</a>';
                    echo '</li>';
                    echo ' </ul>';
                    echo $redak["tekst_usluge"];
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    echo '	</div>';
                }
                ?>



			<!-- <div class="price">$200</div> -->



















<!--

			<div class="col-sm-12 col-lg-4">

<div class="product-item bg-light">
	<div class="card">
		<div class="thumb-content">

			<a href="">
				<img class="card-img-top img-fluid" src="images/products/products-2.jpg" alt="Card image cap">
			</a>
		</div>
		<div class="card-body">
		    <h4 class="card-title"><a href="">Full Study Table Combo</a></h4>
		    <ul class="list-inline product-meta">
		    	<li class="list-inline-item">
		    		<a href=""><i class="fa fa-folder-open-o"></i>Furnitures</a>
		    	</li>
		    	<li class="list-inline-item">
		    		<a href=""><i class="fa fa-calendar"></i>26th December</a>
		    	</li>
		    </ul>
		    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, aliquam!</p>
		    <div class="product-ratings">
		    	<ul class="list-inline">
		    		<li class="list-inline-item selected"><i class="fa fa-star"></i></li>
		    		<li class="list-inline-item selected"><i class="fa fa-star"></i></li>
		    		<li class="list-inline-item selected"><i class="fa fa-star"></i></li>
		    		<li class="list-inline-item selected"><i class="fa fa-star"></i></li>
		    		<li class="list-inline-item"><i class="fa fa-star"></i></li>
		    	</ul>
		    </div>
		</div>
	</div>
</div>



			</div>
			<div class="col-sm-12 col-lg-4">

<div class="product-item bg-light">
	<div class="card">
		<div class="thumb-content">

			<a href="">
				<img class="card-img-top img-fluid" src="images/products/products-3.jpg" alt="Card image cap">
			</a>
		</div>
		<div class="card-body">
		    <h4 class="card-title"><a href="">11inch Macbook Air</a></h4>
		    <ul class="list-inline product-meta">
		    	<li class="list-inline-item">
		    		<a href=""><i class="fa fa-folder-open-o"></i>Electronics</a>
		    	</li>
		    	<li class="list-inline-item">
		    		<a href=""><i class="fa fa-calendar"></i>26th December</a>
		    	</li>
		    </ul>
		    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, aliquam!</p>
		    <div class="product-ratings">
		    	<ul class="list-inline">
		    		<li class="list-inline-item selected"><i class="fa fa-star"></i></li>
		    		<li class="list-inline-item selected"><i class="fa fa-star"></i></li>
		    		<li class="list-inline-item selected"><i class="fa fa-star"></i></li>
		    		<li class="list-inline-item selected"><i class="fa fa-star"></i></li>
		    		<li class="list-inline-item"><i class="fa fa-star"></i></li>
		    	</ul>
		    </div>
		</div>
	</div>
</div>

-->


			
			
		</div>
	</div>
</section>



<!--==========================================
=            All Category Section            =
===========================================-->



  <!-- JAVASCRIPTS -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="plugins/tether/js/tether.min.js"></script>
  <script src="plugins/raty/jquery.raty-fa.js"></script>
  <script src="plugins/bootstrap/dist/js/popper.min.js"></script>
  <script src="plugins/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="plugins/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
  <script src="plugins/slick-carousel/slick/slick.min.js"></script>
  <script src="plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
  <script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
  <script src="plugins/smoothscroll/SmoothScroll.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>
  <script src="js/scripts.js"></script>

</body>

</html>



